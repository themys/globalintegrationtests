"""
This test opens the 4MatsVortex Hercule database, applies the vtkMaterialInterface filter
with the option FillMaterial set to OFF and compares the result for each material to the
reference images stored into the ThemysServerPlugins repository
"""
# trace generated using paraview version 5.11.0-353-g907006070a
#import paraview
#paraview.compatibility.major = 5
#paraview.compatibility.minor = 11
from pathlib import Path

from paraview.simple import *
from paraview.vtk.test import Testing

from common import (PLUGIN_READERS_INSTALL_PATH, PLUGIN_READERS_SOURCE_PATH,
    PLUGIN_THEMYSSERVERPLUGINS_FILTERS_INSTALL_PATH, PLUGIN_THEMYSSERVERPLUGINS_SOURCE_PATH)

# load plugin
LoadPlugin(f'{PLUGIN_READERS_INSTALL_PATH}/CEAHerculeReaders.so', remote=False, ns=globals())

# load plugin
LoadPlugin(f'{PLUGIN_THEMYSSERVERPLUGINS_FILTERS_INSTALL_PATH}/ThemysFilters.so', remote=False, ns=globals())

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

#create a new 'CEA Readers'
hDepnTemps_usp0001 = HerculeServicesReader(registrationName='HDep-n=Temps_u=s.p-0001', FileName=f'{PLUGIN_READERS_SOURCE_PATH}/Plugin/Testing/Data/Interfaces/euler_1proc/HDep-n=Temps_u=s.p-0001')
hDepnTemps_usp0001.FixedTime = ''
hDepnTemps_usp0001.CellDataArray = ['vtkCellId', 'vtkDomainId']
hDepnTemps_usp0001.PointDataArray = ['vtkNodeId']
hDepnTemps_usp0001.MaterialArray = ['[  1] NE', '[  2] NW', '[  3] SE', '[  4] SW']
hDepnTemps_usp0001.MeshArray = ['M1']
hDepnTemps_usp0001.OptionArray = ['Load interface variables']

# get animation scene
animationScene1 = GetAnimationScene()

# Properties modified on animationScene1
animationScene1.AnimationTime = 1.0002804123472189

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# show data in view
hDepnTemps_usp0001Display = Show(hDepnTemps_usp0001, renderView1, 'GeometryRepresentation')

# reset view to fit data
renderView1.ResetCamera(False)

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Material Interface (LOVE)'
materialInterfaceLOVE1 = MaterialInterfaceLOVE(registrationName='MaterialInterfaceLOVE1', Input=hDepnTemps_usp0001)
materialInterfaceLOVE1.MaskArray = ['POINTS', 'None']
materialInterfaceLOVE1.NormalArray2 = ['POINTS', 'None']
materialInterfaceLOVE1.DistanceArray2 = ['POINTS', 'None']
materialInterfaceLOVE1.FillMaterialOn = 0

# show data in view
materialInterfaceLOVE1Display = Show(materialInterfaceLOVE1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
materialInterfaceLOVE1Display.Representation = "Surface"
materialInterfaceLOVE1Display.ColorArrayName = [None, ""]
materialInterfaceLOVE1Display.SelectTCoordArray = "None"
materialInterfaceLOVE1Display.SelectNormalArray = "None"
materialInterfaceLOVE1Display.SelectTangentArray = "None"
materialInterfaceLOVE1Display.OSPRayScaleFunction = "PiecewiseFunction"
materialInterfaceLOVE1Display.SelectOrientationVectors = "None"
materialInterfaceLOVE1Display.ScaleFactor = 0.1
materialInterfaceLOVE1Display.SelectScaleArray = "None"
materialInterfaceLOVE1Display.GlyphType = "Arrow"
materialInterfaceLOVE1Display.GlyphTableIndexArray = "None"
materialInterfaceLOVE1Display.GaussianRadius = 0.005
materialInterfaceLOVE1Display.SetScaleArray = [None, ""]
materialInterfaceLOVE1Display.ScaleTransferFunction = "PiecewiseFunction"
materialInterfaceLOVE1Display.OpacityArray = [None, ""]
materialInterfaceLOVE1Display.OpacityTransferFunction = "PiecewiseFunction"
materialInterfaceLOVE1Display.DataAxesGrid = "GridAxesRepresentation"
materialInterfaceLOVE1Display.PolarAxes = "PolarAxesRepresentation"
materialInterfaceLOVE1Display.SelectInputVectors = [None, ""]
materialInterfaceLOVE1Display.WriteLog = ""

# hide data in view
Hide(hDepnTemps_usp0001, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(materialInterfaceLOVE1Display, ('FIELD', 'vtkBlockColors'))

# show color bar/color legend
materialInterfaceLOVE1Display.SetScalarBarVisibility(renderView1, True)

# Properties modified on materialInterfaceLOVE1Display
materialInterfaceLOVE1Display.PointSize = 5.0

# Properties modified on materialInterfaceLOVE1Display
materialInterfaceLOVE1Display.LineWidth = 5.0

# Properties modified on materialInterfaceLOVE1Display
materialInterfaceLOVE1Display.BlockSelectors = [
    "/Root/M1/_1NE",
]

# get layout
layout1 = GetLayout()

# layout/tab size in pixels
layout1.SetSize(1482, 867)

# current camera placement for renderView1
renderView1.InteractionMode = "2D"
renderView1.CameraPosition = [0.5, 0.5, 10000.0]
renderView1.CameraFocalPoint = [0.5, 0.5, 0.0]
renderView1.CameraParallelScale = 0.7071067811865476

baseline_path = Path(f"{PLUGIN_THEMYSSERVERPLUGINS_SOURCE_PATH}") / "Data" / "Baseline" 
baseline_file = baseline_path / "MaterialFilters" / "4matsvortex_fill_material_off_1NE.png"
Testing.compareImage(renderView1.GetRenderWindow(), baseline_file.as_posix(), threshold=0.0)
Testing.interact()

# Properties modified on materialInterfaceLOVE1Display
materialInterfaceLOVE1Display.BlockSelectors = [
    "/Root/M1/_2NW",
]

baseline_file = baseline_path / "MaterialFilters" / "4matsvortex_fill_material_off_2NW.png"
Testing.compareImage(renderView1.GetRenderWindow(), baseline_file.as_posix(), threshold=0.0)
Testing.interact()

# Properties modified on materialInterfaceLOVE1Display
materialInterfaceLOVE1Display.BlockSelectors = [
    "/Root/M1/_3SE",
]

baseline_file = baseline_path / "MaterialFilters" / "4matsvortex_fill_material_off_3SE.png"
Testing.compareImage(renderView1.GetRenderWindow(), baseline_file.as_posix(), threshold=0.0)
Testing.interact()

# Properties modified on materialInterfaceLOVE1Display
materialInterfaceLOVE1Display.BlockSelectors = [
    "/Root/M1/_4SW",
]

baseline_file = baseline_path / "MaterialFilters" / "4matsvortex_fill_material_off_4SW.png"
Testing.compareImage(renderView1.GetRenderWindow(), baseline_file.as_posix(), threshold=0.0)
Testing.interact()