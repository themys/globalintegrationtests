"""
This module tests that the ContourWriter filter is functional.

It loads a Hercule database made of 4 mats (vortex) computed on 8 procs.
It applies a filter to convert the MultiBlockDataSet into a PartitionnedDataSet,
another to build the ghost cells and then its saves the contour using the 
ContourWriter filter.
"""
#paraview.compatibility.major = 5
#paraview.compatibility.minor = 11

from pathlib import Path
import sys
from tempfile import NamedTemporaryFile
#### import the simple module from the paraview
from paraview.simple import *
from paraview.vtk.test import Testing

from common import (PLUGIN_READERS_INSTALL_PATH, PLUGIN_READERS_SOURCE_PATH,
    PLUGIN_THEMYSSERVERPLUGINS_FILTERS_INSTALL_PATH, BASELINE_DIR)

# load plugin
LoadPlugin(f'{PLUGIN_READERS_INSTALL_PATH}/CEAHerculeReaders.so', remote=False, ns=globals())

# load plugin
LoadPlugin(f'{PLUGIN_THEMYSSERVERPLUGINS_FILTERS_INSTALL_PATH}/ThemysFilters.so', remote=False, ns=globals())

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

HDEP_NAME ="4mats_vortex_8procs" 
# create a new 'XML MultiBlock Data Reader'
a4mats_vortex_8procsvtm = HerculeServicesReader(registrationName=f'{HDEP_NAME}.vtm', FileName=[f'{PLUGIN_READERS_SOURCE_PATH}/Plugin/Testing/Data/Interfaces/euler_8procs/HDep-n=Temps_u=s+n=NumSDom_g=0000x0007.p-0001'])

# get animation scene
animationScene1 = GetAnimationScene()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# show data in view
a4mats_vortex_8procsvtmDisplay = Show(a4mats_vortex_8procsvtm, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
a4mats_vortex_8procsvtmDisplay.Representation = 'Surface'

# reset view to fit data
renderView1.ResetCamera(False, 0.9)

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(a4mats_vortex_8procsvtmDisplay, ('FIELD', 'vtkBlockColors'))

# show color bar/color legend
a4mats_vortex_8procsvtmDisplay.SetScalarBarVisibility(renderView1, True)

# create a new 'Convert To PartitionedDataSetCollection'
convertToPartitionedDataSetCollection1 = ConvertToPartitionedDataSetCollection(registrationName='ConvertToPartitionedDataSetCollection1', Input=a4mats_vortex_8procsvtm)

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# show data in view
convertToPartitionedDataSetCollection1Display = Show(convertToPartitionedDataSetCollection1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
convertToPartitionedDataSetCollection1Display.Representation = 'Surface'

# hide data in view
Hide(a4mats_vortex_8procsvtm, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(convertToPartitionedDataSetCollection1Display, ('FIELD', 'vtkBlockColors'))

# show color bar/color legend
convertToPartitionedDataSetCollection1Display.SetScalarBarVisibility(renderView1, True)

# create a new 'Ghost Cells Generator'
ghostCellsGenerator1 = GhostCellsGenerator(registrationName='GhostCellsGenerator1', Input=convertToPartitionedDataSetCollection1)

# show data in view
ghostCellsGenerator1Display = Show(ghostCellsGenerator1, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
ghostCellsGenerator1Display.Representation = 'Surface'

# hide data in view
Hide(convertToPartitionedDataSetCollection1, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(ghostCellsGenerator1Display, ('FIELD', 'vtkBlockColors'))

# show color bar/color legend
ghostCellsGenerator1Display.SetScalarBarVisibility(renderView1, True)

# Properties modified on animationScene1
animationScene1.AnimationTime = 1.0002804123472189

with NamedTemporaryFile(mode='a+', delete=False, suffix=".dat") as result:
    print(f"Temporary file: {result.name}")
    # Save the contour
    SaveData(result.name, proxy=ghostCellsGenerator1)

try:
    # create a new 'CEA Reader DAT (Python)'
    contour_4mats_vortexdat = CEAReaderDATPython(registrationName='contour_4mats_vortex.dat', FileName=result.name)
except:
    print(f"Unable to build the CEAReaderDATPython object from the file {result.name}", file=sys.stderr)
    raise

# show data in view
contour_4mats_vortexdatDisplay = Show(contour_4mats_vortexdat, renderView1, 'GeometryRepresentation')

Hide(ghostCellsGenerator1)

# trace defaults for the display properties.
contour_4mats_vortexdatDisplay.Representation = 'Surface'
contour_4mats_vortexdatDisplay.ColorArrayName = [None, '']
contour_4mats_vortexdatDisplay.SelectTCoordArray = 'None'
contour_4mats_vortexdatDisplay.SelectNormalArray = 'None'
contour_4mats_vortexdatDisplay.SelectTangentArray = 'None'
contour_4mats_vortexdatDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
contour_4mats_vortexdatDisplay.SelectOrientationVectors = 'None'
contour_4mats_vortexdatDisplay.ScaleFactor = 0.1
contour_4mats_vortexdatDisplay.SelectScaleArray = 'None'
contour_4mats_vortexdatDisplay.GlyphType = 'Arrow'
contour_4mats_vortexdatDisplay.GlyphTableIndexArray = 'None'
contour_4mats_vortexdatDisplay.GaussianRadius = 0.005
contour_4mats_vortexdatDisplay.SetScaleArray = [None, '']
contour_4mats_vortexdatDisplay.ScaleTransferFunction = 'PiecewiseFunction'
contour_4mats_vortexdatDisplay.OpacityArray = [None, '']
contour_4mats_vortexdatDisplay.OpacityTransferFunction = 'PiecewiseFunction'
contour_4mats_vortexdatDisplay.DataAxesGrid = 'GridAxesRepresentation'
contour_4mats_vortexdatDisplay.PolarAxes = 'PolarAxesRepresentation'
contour_4mats_vortexdatDisplay.SelectInputVectors = [None, '']
contour_4mats_vortexdatDisplay.WriteLog = ''

# update the view to ensure updated data information
renderView1.Update()

#================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
#================================================================

# get layout
layout1 = GetLayout()

#--------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
layout1.SetSize(959, 536)

#-----------------------------------
# saving camera placements for views

# current camera placement for renderView1
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.5, 0.5, 10000.0]
renderView1.CameraFocalPoint = [0.5, 0.5, 0.0]
renderView1.CameraParallelScale = 0.7071067811865476
baseline_file = Path(BASELINE_DIR) / "check_integration_with_contourwriter_parallel.png"
Testing.compareImage(renderView1.GetRenderWindow(), baseline_file.as_posix(), threshold=0.0)
Testing.interact()