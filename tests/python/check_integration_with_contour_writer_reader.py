"""
This test opens the 4MatsVortex Hercule database, saves its contour in a temporary
file and then opens this temporary file to display the contour obtained.
The result is then compared to the reference image stored into this repository data/baseline directory.
"""
# trace generated using paraview version 5.11.0-353-g907006070a
#import paraview
#paraview.compatibility.major = 5
#paraview.compatibility.minor = 11

from pathlib import Path
from tempfile import NamedTemporaryFile

from paraview.simple import *
from paraview.vtk.test import Testing

from common import (PLUGIN_READERS_INSTALL_PATH, PLUGIN_READERS_SOURCE_PATH,
    PLUGIN_THEMYSSERVERPLUGINS_FILTERS_INSTALL_PATH, BASELINE_DIR)

# load plugin
LoadPlugin(f'{PLUGIN_READERS_INSTALL_PATH}/CEAHerculeReaders.so', remote=False, ns=globals())

# load plugin
LoadPlugin(f'{PLUGIN_THEMYSSERVERPLUGINS_FILTERS_INSTALL_PATH}/ThemysFilters.so', remote=False, ns=globals())

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'CEA Readers'
hDepnTemps_usp0001 = HerculeServicesReader(registrationName='HDep-n=Temps_u=s.p-0001', FileName=f'{PLUGIN_READERS_SOURCE_PATH}/Plugin/Testing/Data/Interfaces/euler_1proc/HDep-n=Temps_u=s.p-0001')
hDepnTemps_usp0001.FixedTime = ''
hDepnTemps_usp0001.CellDataArray = ['vtkCellId', 'vtkDomainId']
hDepnTemps_usp0001.PointDataArray = ['vtkNodeId']
hDepnTemps_usp0001.MaterialArray = ['[  0] BLOCVACUUM', '[  1] NE', '[  2] NW', '[  3] SE', '[  4] SW']
hDepnTemps_usp0001.MeshArray = ['M1']
hDepnTemps_usp0001.OptionArray = []

# get animation scene
animationScene1 = GetAnimationScene()

# get the time-keeper
timeKeeper1 = GetTimeKeeper()

# update animation scene based on data timesteps
animationScene1.UpdateAnimationUsingDataTimeSteps()

# rename source object
RenameSource('HDepnTemps_usp0001', hDepnTemps_usp0001)

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')

# show data in view
hDepnTemps_usp0001Display = Show(hDepnTemps_usp0001, renderView1, 'GeometryRepresentation')

# trace defaults for the display properties.
hDepnTemps_usp0001Display.Representation = 'Surface'
hDepnTemps_usp0001Display.ColorArrayName = [None, '']
hDepnTemps_usp0001Display.SelectTCoordArray = 'None'
hDepnTemps_usp0001Display.SelectNormalArray = 'None'
hDepnTemps_usp0001Display.SelectTangentArray = 'None'
hDepnTemps_usp0001Display.OSPRayScaleArray = 'vtkInternalGlobalNodeId'
hDepnTemps_usp0001Display.OSPRayScaleFunction = 'PiecewiseFunction'
hDepnTemps_usp0001Display.SelectOrientationVectors = 'None'
hDepnTemps_usp0001Display.ScaleFactor = 0.1
hDepnTemps_usp0001Display.SelectScaleArray = 'None'
hDepnTemps_usp0001Display.GlyphType = 'Arrow'
hDepnTemps_usp0001Display.GlyphTableIndexArray = 'None'
hDepnTemps_usp0001Display.GaussianRadius = 0.005
hDepnTemps_usp0001Display.SetScaleArray = ['POINTS', 'vtkInternalGlobalNodeId']
hDepnTemps_usp0001Display.ScaleTransferFunction = 'PiecewiseFunction'
hDepnTemps_usp0001Display.OpacityArray = ['POINTS', 'vtkInternalGlobalNodeId']
hDepnTemps_usp0001Display.OpacityTransferFunction = 'PiecewiseFunction'
hDepnTemps_usp0001Display.DataAxesGrid = 'GridAxesRepresentation'
hDepnTemps_usp0001Display.PolarAxes = 'PolarAxesRepresentation'
hDepnTemps_usp0001Display.SelectInputVectors = [None, '']
hDepnTemps_usp0001Display.WriteLog = ''

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
hDepnTemps_usp0001Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 2600.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
hDepnTemps_usp0001Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 2600.0, 1.0, 0.5, 0.0]

# reset view to fit data
renderView1.ResetCamera(False)

#changing interaction mode based on data extents
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.5, 0.5, 10000.0]
renderView1.CameraFocalPoint = [0.5, 0.5, 0.0]

# update the view to ensure updated data information
renderView1.Update()

# set scalar coloring
ColorBy(hDepnTemps_usp0001Display, ('FIELD', 'vtkBlockColors'))

# show color bar/color legend
hDepnTemps_usp0001Display.SetScalarBarVisibility(renderView1, True)

# get color transfer function/color map for 'vtkBlockColors'
vtkBlockColorsLUT = GetColorTransferFunction('vtkBlockColors')

# get opacity transfer function/opacity map for 'vtkBlockColors'
vtkBlockColorsPWF = GetOpacityTransferFunction('vtkBlockColors')

# get 2D transfer function for 'vtkBlockColors'
vtkBlockColorsTF2D = GetTransferFunction2D('vtkBlockColors')

# Properties modified on animationScene1
animationScene1.AnimationTime = 1.0002804123472189

with NamedTemporaryFile(mode='a+', delete=False, suffix=".dat") as result:
    print(f"Temporary file: {result.name}")
    # Save the contour
    SaveData(result.name, proxy=hDepnTemps_usp0001)

    # create a new 'CEA Reader DAT (Python)'
    contour_4mats_vortexdat = CEAReaderDATPython(registrationName='contour_4mats_vortex.dat', FileName=result.name)

# show data in view
contour_4mats_vortexdatDisplay = Show(contour_4mats_vortexdat, renderView1, 'GeometryRepresentation')

Hide(hDepnTemps_usp0001)

# trace defaults for the display properties.
contour_4mats_vortexdatDisplay.Representation = 'Surface'
contour_4mats_vortexdatDisplay.ColorArrayName = [None, '']
contour_4mats_vortexdatDisplay.SelectTCoordArray = 'None'
contour_4mats_vortexdatDisplay.SelectNormalArray = 'None'
contour_4mats_vortexdatDisplay.SelectTangentArray = 'None'
contour_4mats_vortexdatDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
contour_4mats_vortexdatDisplay.SelectOrientationVectors = 'None'
contour_4mats_vortexdatDisplay.ScaleFactor = 0.1
contour_4mats_vortexdatDisplay.SelectScaleArray = 'None'
contour_4mats_vortexdatDisplay.GlyphType = 'Arrow'
contour_4mats_vortexdatDisplay.GlyphTableIndexArray = 'None'
contour_4mats_vortexdatDisplay.GaussianRadius = 0.005
contour_4mats_vortexdatDisplay.SetScaleArray = [None, '']
contour_4mats_vortexdatDisplay.ScaleTransferFunction = 'PiecewiseFunction'
contour_4mats_vortexdatDisplay.OpacityArray = [None, '']
contour_4mats_vortexdatDisplay.OpacityTransferFunction = 'PiecewiseFunction'
contour_4mats_vortexdatDisplay.DataAxesGrid = 'GridAxesRepresentation'
contour_4mats_vortexdatDisplay.PolarAxes = 'PolarAxesRepresentation'
contour_4mats_vortexdatDisplay.SelectInputVectors = [None, '']
contour_4mats_vortexdatDisplay.WriteLog = ''

# update the view to ensure updated data information
renderView1.Update()

#================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
#================================================================

# get layout
layout1 = GetLayout()

#--------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
layout1.SetSize(959, 536)

#-----------------------------------
# saving camera placements for views

# current camera placement for renderView1
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.5, 0.5, 10000.0]
renderView1.CameraFocalPoint = [0.5, 0.5, 0.0]
renderView1.CameraParallelScale = 0.7071067811865476

#--------------------------------------------
# uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).

baseline_file = Path(BASELINE_DIR) / "pluginspython_ceareaderdat.png"
Testing.compareImage(renderView1.GetRenderWindow(), baseline_file.as_posix(), threshold=0.0)
Testing.interact()