"""
This test opens the 4MatsVortex Hercule database, applies the vtkMaterialInterface filter
and a reflect around y plane and then compares the result to the reference image stored into
the baseline directory

Identical to check_integration_with_materialinterfacefilter.py except the suplementary reflect operation
and the fact that the reference image is stored in this repository
"""
# trace generated using paraview version 5.11.0-353-g907006070a
#import paraview
#paraview.compatibility.major = 5
#paraview.compatibility.minor = 11
from pathlib import Path

from paraview.simple import *
from paraview.vtk.test import Testing

from common import (PLUGIN_READERS_INSTALL_PATH, PLUGIN_READERS_SOURCE_PATH,
    PLUGIN_THEMYSSERVERPLUGINS_FILTERS_INSTALL_PATH, BASELINE_DIR)

# load plugin
LoadPlugin(f'{PLUGIN_READERS_INSTALL_PATH}/CEAHerculeReaders.so', remote=False, ns=globals())

# load plugin
LoadPlugin(f'{PLUGIN_THEMYSSERVERPLUGINS_FILTERS_INSTALL_PATH}/ThemysFilters.so', remote=False, ns=globals())

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'CEA Readers'
database_to_refimg = (('euler_1proc/HDep-n=Temps_u=s.p-0001', ['[  1] NE', '[  2] NW', '[  3] SE', '[  4] SW'], '4MatsVortex_add_reflect_euler_1proc.png'),
                      ('ale_8procs/HDep-n=Temps_u=s+n=NumSDom_g=0000x0007.p-0001', ['[  2] NE', '[  3] NW', '[  4] SE', '[  5] SW'], '4MatsVortex_add_reflect_ale_8procs.png'))
for db_path, mat_array, refimg in database_to_refimg:
    hDepnTemps_usp0001 = HerculeServicesReader(registrationName='HDep-n=Temps_u=s.p-0001', FileName=f'{PLUGIN_READERS_SOURCE_PATH}/Plugin/Testing/Data/Interfaces/{db_path}')
    hDepnTemps_usp0001.FixedTime = ''
    hDepnTemps_usp0001.CellDataArray = ['vtkCellId', 'vtkDomainId']
    hDepnTemps_usp0001.PointDataArray = ['vtkNodeId']
    hDepnTemps_usp0001.MaterialArray = mat_array
    hDepnTemps_usp0001.MeshArray = ['M1']
    hDepnTemps_usp0001.OptionArray = ['Load interface variables']

    # get animation scene
    animationScene1 = GetAnimationScene()

    # get the time-keeper
    timeKeeper1 = GetTimeKeeper()

    # update animation scene based on data timesteps
    animationScene1.UpdateAnimationUsingDataTimeSteps()

    # get active view
    renderView1 = GetActiveViewOrCreate('RenderView')

    # show data in view
    hDepnTemps_usp0001Display = Show(hDepnTemps_usp0001, renderView1, 'GeometryRepresentation')

    # trace defaults for the display properties.
    hDepnTemps_usp0001Display.Representation = 'Surface'
    hDepnTemps_usp0001Display.ColorArrayName = [None, '']
    hDepnTemps_usp0001Display.SelectTCoordArray = 'None'
    hDepnTemps_usp0001Display.SelectNormalArray = 'None'
    hDepnTemps_usp0001Display.SelectTangentArray = 'None'
    hDepnTemps_usp0001Display.OSPRayScaleArray = 'vtkInternalGlobalNodeId'
    hDepnTemps_usp0001Display.OSPRayScaleFunction = 'PiecewiseFunction'
    hDepnTemps_usp0001Display.SelectOrientationVectors = 'None'
    hDepnTemps_usp0001Display.ScaleFactor = 0.1
    hDepnTemps_usp0001Display.SelectScaleArray = 'None'
    hDepnTemps_usp0001Display.GlyphType = 'Arrow'
    hDepnTemps_usp0001Display.GlyphTableIndexArray = 'None'
    hDepnTemps_usp0001Display.GaussianRadius = 0.005
    hDepnTemps_usp0001Display.SetScaleArray = ['POINTS', 'vtkInternalGlobalNodeId']
    hDepnTemps_usp0001Display.ScaleTransferFunction = 'PiecewiseFunction'
    hDepnTemps_usp0001Display.OpacityArray = ['POINTS', 'vtkInternalGlobalNodeId']
    hDepnTemps_usp0001Display.OpacityTransferFunction = 'PiecewiseFunction'
    hDepnTemps_usp0001Display.DataAxesGrid = 'GridAxesRepresentation'
    hDepnTemps_usp0001Display.PolarAxes = 'PolarAxesRepresentation'
    hDepnTemps_usp0001Display.SelectInputVectors = [None, '']
    hDepnTemps_usp0001Display.WriteLog = ''

    # init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
    hDepnTemps_usp0001Display.ScaleTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 2600.0, 1.0, 0.5, 0.0]

    # init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
    hDepnTemps_usp0001Display.OpacityTransferFunction.Points = [0.0, 0.0, 0.5, 0.0, 2600.0, 1.0, 0.5, 0.0]

    # reset view to fit data
    renderView1.ResetCamera(False)

    #changing interaction mode based on data extents
    renderView1.InteractionMode = '2D'
    renderView1.CameraPosition = [0.5, 0.5, 10000.0]
    renderView1.CameraFocalPoint = [0.5, 0.5, 0.0]

    # update the view to ensure updated data information
    renderView1.Update()

    # set scalar coloring
    ColorBy(hDepnTemps_usp0001Display, ('FIELD', 'vtkBlockColors'))

    # show color bar/color legend
    hDepnTemps_usp0001Display.SetScalarBarVisibility(renderView1, True)

    # get color transfer function/color map for 'vtkBlockColors'
    vtkBlockColorsLUT = GetColorTransferFunction('vtkBlockColors')

    # get opacity transfer function/opacity map for 'vtkBlockColors'
    vtkBlockColorsPWF = GetOpacityTransferFunction('vtkBlockColors')

    # get 2D transfer function for 'vtkBlockColors'
    vtkBlockColorsTF2D = GetTransferFunction2D('vtkBlockColors')

    # Properties modified on animationScene1
    animationScene1.AnimationTime = 1.0002804123472189

    # create a new 'Material Interface (LOVE)'
    materialInterfaceLOVE1 = MaterialInterfaceLOVE(registrationName='MaterialInterfaceLOVE1', Input=hDepnTemps_usp0001)
    materialInterfaceLOVE1.MaskArray = ['POINTS', 'None']
    materialInterfaceLOVE1.NormalArray2 = ['POINTS', 'None']
    materialInterfaceLOVE1.DistanceArray2 = ['POINTS', 'None']

    # show data in view
    materialInterfaceLOVE1Display = Show(materialInterfaceLOVE1, renderView1, 'UnstructuredGridRepresentation')

    # trace defaults for the display properties.
    materialInterfaceLOVE1Display.Representation = 'Surface'
    materialInterfaceLOVE1Display.ColorArrayName = [None, '']
    materialInterfaceLOVE1Display.SelectTCoordArray = 'None'
    materialInterfaceLOVE1Display.SelectNormalArray = 'None'
    materialInterfaceLOVE1Display.SelectTangentArray = 'None'
    materialInterfaceLOVE1Display.OSPRayScaleFunction = 'PiecewiseFunction'
    materialInterfaceLOVE1Display.SelectOrientationVectors = 'None'
    materialInterfaceLOVE1Display.ScaleFactor = 0.1
    materialInterfaceLOVE1Display.SelectScaleArray = 'None'
    materialInterfaceLOVE1Display.GlyphType = 'Arrow'
    materialInterfaceLOVE1Display.GlyphTableIndexArray = 'None'
    materialInterfaceLOVE1Display.GaussianRadius = 0.005
    materialInterfaceLOVE1Display.SetScaleArray = [None, '']
    materialInterfaceLOVE1Display.ScaleTransferFunction = 'PiecewiseFunction'
    materialInterfaceLOVE1Display.OpacityArray = [None, '']
    materialInterfaceLOVE1Display.OpacityTransferFunction = 'PiecewiseFunction'
    materialInterfaceLOVE1Display.DataAxesGrid = 'GridAxesRepresentation'
    materialInterfaceLOVE1Display.PolarAxes = 'PolarAxesRepresentation'
    materialInterfaceLOVE1Display.ScalarOpacityUnitDistance = 0.09257530532948319
    materialInterfaceLOVE1Display.OpacityArrayName = ['CELLS', 'vtkCellId']
    materialInterfaceLOVE1Display.SelectInputVectors = [None, '']
    materialInterfaceLOVE1Display.WriteLog = ''

    # hide data in view
    Hide(hDepnTemps_usp0001, renderView1)

    # update the view to ensure updated data information
    renderView1.Update()

    # set scalar coloring
    ColorBy(materialInterfaceLOVE1Display, ('FIELD', 'vtkBlockColors'))

    # show color bar/color legend
    materialInterfaceLOVE1Display.SetScalarBarVisibility(renderView1, True)

    # create a new 'ReflectY'
    reflectY1 = ReflectY(registrationName='ReflectY1', Input=materialInterfaceLOVE1)

    # get active view
    renderView1 = GetActiveViewOrCreate('RenderView')

    # show data in view
    reflectY1Display = Show(reflectY1, renderView1, 'UnstructuredGridRepresentation')

    # trace defaults for the display properties.
    reflectY1Display.Representation = 'Surface'
    reflectY1Display.ColorArrayName = [None, '']
    reflectY1Display.SelectNormalArray = 'None'
    reflectY1Display.SelectTangentArray = 'None'
    reflectY1Display.SelectTCoordArray = 'None'
    reflectY1Display.TextureTransform = 'Transform2'
    reflectY1Display.OSPRayScaleFunction = 'Piecewise Function'
    reflectY1Display.Assembly = 'Hierarchy'
    reflectY1Display.SelectOrientationVectors = 'None'
    reflectY1Display.ScaleFactor = 0.1
    reflectY1Display.SelectScaleArray = 'None'
    reflectY1Display.GlyphType = 'Arrow'
    reflectY1Display.GlyphTableIndexArray = 'None'
    reflectY1Display.GaussianRadius = 0.005
    reflectY1Display.SetScaleArray = [None, '']
    reflectY1Display.ScaleTransferFunction = 'Piecewise Function'
    reflectY1Display.OpacityArray = [None, '']
    reflectY1Display.OpacityTransferFunction = 'Piecewise Function'
    reflectY1Display.DataAxesGrid = 'Grid Axes Representation'
    reflectY1Display.PolarAxes = 'Polar Axes Representation'
    reflectY1Display.ScalarOpacityUnitDistance = 0.09257530532948319
    reflectY1Display.OpacityArrayName = ['CELLS', 'vtkCellId']
    reflectY1Display.SelectInputVectors = [None, '']
    reflectY1Display.WriteLog = ''

    # hide data in view
    Hide(materialInterfaceLOVE1, renderView1)

    # update the view to ensure updated data information
    renderView1.Update()

    # set scalar coloring
    ColorBy(reflectY1Display, ('FIELD', 'vtkBlockColors'))

    # show color bar/color legend
    reflectY1Display.SetScalarBarVisibility(renderView1, True)

    # get color transfer function/color map for 'vtkBlockColors'
    vtkBlockColorsLUT = GetColorTransferFunction('vtkBlockColors')

    # get opacity transfer function/opacity map for 'vtkBlockColors'
    vtkBlockColorsPWF = GetOpacityTransferFunction('vtkBlockColors')

    # get 2D transfer function for 'vtkBlockColors'
    vtkBlockColorsTF2D = GetTransferFunction2D('vtkBlockColors')

    # set active source
    SetActiveSource(materialInterfaceLOVE1)

    # show data in view
    materialInterfaceLOVE1Display = Show(materialInterfaceLOVE1, renderView1, 'UnstructuredGridRepresentation')

    # trace defaults for the display properties.
    materialInterfaceLOVE1Display.Representation = 'Surface'
    materialInterfaceLOVE1Display.ColorArrayName = ['FIELD', 'vtkBlockColors']
    materialInterfaceLOVE1Display.LookupTable = vtkBlockColorsLUT
    materialInterfaceLOVE1Display.SelectNormalArray = 'None'
    materialInterfaceLOVE1Display.SelectTangentArray = 'None'
    materialInterfaceLOVE1Display.SelectTCoordArray = 'None'
    materialInterfaceLOVE1Display.TextureTransform = 'Transform2'
    materialInterfaceLOVE1Display.OSPRayScaleFunction = 'Piecewise Function'
    materialInterfaceLOVE1Display.Assembly = 'Hierarchy'
    materialInterfaceLOVE1Display.SelectOrientationVectors = 'None'
    materialInterfaceLOVE1Display.ScaleFactor = 0.1
    materialInterfaceLOVE1Display.SelectScaleArray = 'None'
    materialInterfaceLOVE1Display.GlyphType = 'Arrow'
    materialInterfaceLOVE1Display.GlyphTableIndexArray = 'None'
    materialInterfaceLOVE1Display.GaussianRadius = 0.005
    materialInterfaceLOVE1Display.SetScaleArray = ['POINTS', '']
    materialInterfaceLOVE1Display.ScaleTransferFunction = 'Piecewise Function'
    materialInterfaceLOVE1Display.OpacityArray = ['POINTS', '']
    materialInterfaceLOVE1Display.OpacityTransferFunction = 'Piecewise Function'
    materialInterfaceLOVE1Display.DataAxesGrid = 'Grid Axes Representation'
    materialInterfaceLOVE1Display.PolarAxes = 'Polar Axes Representation'
    materialInterfaceLOVE1Display.ScalarOpacityFunction = vtkBlockColorsPWF
    materialInterfaceLOVE1Display.ScalarOpacityUnitDistance = 0.09257530532948319
    materialInterfaceLOVE1Display.OpacityArrayName = ['CELLS', 'vtkCellId']
    materialInterfaceLOVE1Display.SelectInputVectors = ['POINTS', '']
    materialInterfaceLOVE1Display.WriteLog = ''

    # show color bar/color legend
    materialInterfaceLOVE1Display.SetScalarBarVisibility(renderView1, True)

    # reset view to fit data
    renderView1.ResetCamera(False, 0.9)

    #================================================================
    # addendum: following script captures some of the application
    # state to faithfully reproduce the visualization during playback
    #================================================================

    # get layout
    layout1 = GetLayout()

    #--------------------------------
    # saving layout sizes for layouts

    # layout/tab size in pixels
    layout1.SetSize(300, 300)

    #-----------------------------------
    # saving camera placements for views

    # current camera placement for renderView1
    renderView1.InteractionMode = '2D'
    renderView1.CameraPosition = [0.5, 0.0, 4.319751617610021]
    renderView1.CameraFocalPoint = [0.5, 0.0, 0.0]
    renderView1.CameraParallelScale = 1.118033988749895

    baseline_file = Path(BASELINE_DIR) / refimg
    Testing.compareImage(renderView1.GetRenderWindow(), baseline_file.as_posix(), threshold=0.0)
    Testing.interact()

