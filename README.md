# GlobalIntegrationTests

GlobalIntegrationTests is a repository that realizes superbuild of Themys and
Readers projects to tests the compatibility of Readers development with 
Themys development.

## Installation

To run the inegration tests, first clone this repository:

```bash
git clone https://gitlab.com/themys/globalintegrationtests.git
```

Then create a build directory and descend into it:

```bash
mkdir -p globalintegrationtests_build
cd globalintegrationtests_build
```

Then configure the project and build it with:

```bash
cmake -DHERCULE_INSTALL_PATH=/path/to/hercule_install/lib ../globalintegrationtests
cmake --build -j 8
```

During the build phase, `CMake` will fetch, configure, build and install the sources.
By default, the `master` branches of each project is fetched. If a specific tag
is required for the readers or themys project, then run:

```bash
cmake -DHERCULE_INSTALL_PATH=/path/to/hercule_install/lib -DREADERS_GIT_TAG="1.0.2" -DTHEMYS_GIT_TAG="1.0.1" ../globalintegrationtests
cmake --build -j 8
```

Once built, the integration tests can be run through the following command:

```bash
ctest -j 8
```

## Purpose of a test

The goal of a test is to open an Hercule database, apply some filters on it and 
compare the result to a result stored into **Readers** or **ThemysServerPlugins** repository
or stored into this repository `data/baseline`'s directory.

## How to add a test

To add a test, use `ParaView`'s macro to record the operations the test is supposed to do.

Then save the file into the `globalintegrationtests/tests/python`.

Modify this file to add the following lines at the beginning (just after the line `from paraview.simple import *`):

```python
from paraview.vtk.test import Testing

from common import (PLUGIN_READERS_INSTALL_PATH, PLUGIN_READERS_SOURCE_PATH,
    PLUGIN_THEMYSSERVERPLUGINS_FILTERS_INSTALL_PATH, BASELINE_DIR)

# load plugin
LoadPlugin(f'{PLUGIN_READERS_INSTALL_PATH}/CEAHerculeReaders.so', remote=False, ns=globals())

# load plugin
LoadPlugin(f'{PLUGIN_THEMYSSERVERPLUGINS_FILTERS_INSTALL_PATH}/ThemysFilters.so', remote=False, ns=globals())
```

Add the following lines at the end of the test file:

```python
baseline_file = Path(BASELINE_DIR) / "pluginspython_ceareaderdat.png"
Testing.compareImage(renderView1.GetRenderWindow(), baseline_file.as_posix(), threshold=0.0)
Testing.interact()
```

Those lines trigger a comparison between the render window and the image (here named `pluginspython_ceareaderdat`)
stored inside this repository `data/baseline`'s directory.

If the reference image already exists inside **Readers** or **ThemysServerPlugins** repository, then
the baseline file variable should be defined along those lines:

```python
baseline_path = Path(f"{PLUGIN_THEMYS_SOURCE_PATH}") / "plugins" / "Plugins" / "Data" / "Baseline" 
baseline_file = baseline_path / "MaterialFilters_vtkMaterialInterface_4MatsVortex.png"
```

Once done, do not forget to register the test in the `PythonTestNames` list of the `globalintegrationtests/tests/python/CMakeLists.txt` file.

```
set(PythonTestNames
check_integration_with_materialinterfacefilter
check_integration_with_contour_writer_reader
... 
)
```