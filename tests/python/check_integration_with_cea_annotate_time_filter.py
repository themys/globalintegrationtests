"""
This test opens an Hercule DataBase and applies the CEAAnnotateTime filter in order
to check that this last one is able to display the registration name of the source
"""
# trace generated using paraview version 5.13.1-703-g6ed5b78f1a
#import paraview
#paraview.compatibility.major = 5
#paraview.compatibility.minor = 13

#### import the simple module from the paraview
from pathlib import Path

from paraview.simple import *
from paraview.vtk.test import Testing

from common import (PLUGIN_READERS_INSTALL_PATH, PLUGIN_READERS_SOURCE_PATH,
    PLUGIN_PYTHON_INSTALL_PATH, BASELINE_DIR)

# WARNING: order of the loaded plugins matters!
# First load CEAHerculeReaders in order to have correct LD_LIBRARY_PATH
# so that libHerculeServicesReaders.so is found when importing the python module CEAAnnotateTime 
LoadPlugin(f'{PLUGIN_READERS_INSTALL_PATH}/CEAHerculeReaders.so', remote=False, ns=globals())
LoadPlugin(f'{PLUGIN_PYTHON_INSTALL_PATH}/CEAAnnotateTime.py', remote=False, ns=globals())

#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# create a new 'Hercule Services Reader'
READER = HerculeServicesReader(registrationName='HDepnTemps_usnNumSDom_g0000x0001j0000', FileName=f'{PLUGIN_READERS_SOURCE_PATH}/Plugin/Testing/Data/Interfaces/euler_2procs/HDep-n=Temps_u=s+n=NumSDom_g=0000x0001.j-0000')

# get animation scene
ANIMATION_SCENE = GetAnimationScene()

# update animation scene based on data timesteps
ANIMATION_SCENE.UpdateAnimationUsingDataTimeSteps()

# Properties modified on hDepnTemps_usnNumSDom_g0000x0001j0000
READER.CellDataArray = ['Milieu:Density', 'Milieu:Pressure']

# get active view
RENDER_VIEW = GetActiveViewOrCreate('RenderView')

# show data in view
READER_DISPLAY = Show(READER, RENDER_VIEW, 'GeometryRepresentation')

# trace defaults for the display properties.
READER_DISPLAY.Representation = 'Surface'

# reset view to fit data
RENDER_VIEW.ResetCamera(False, 0.9)

# update the view to ensure updated data information
RENDER_VIEW.Update()

# set scalar coloring
ColorBy(READER_DISPLAY, ('FIELD', 'vtkBlockColors'))

# show color bar/color legend
READER_DISPLAY.SetScalarBarVisibility(RENDER_VIEW, True)

# get color transfer function/color map for 'vtkBlockColors'
VTK_BLOCK_COLORS_LUT = GetColorTransferFunction('vtkBlockColors')

# create a new 'CEA AnnotateTime'
CEA_ANNOTATE_TIME = CEAAnnotateTime(registrationName='CEAAnnotateTime1', Input=READER)

# show data in view
CEA_ANNOTATE_TIME_DISPLAY = Show(CEA_ANNOTATE_TIME, RENDER_VIEW, 'TextSourceRepresentation')

# update the view to ensure updated data information
RENDER_VIEW.Update()

ANIMATION_SCENE.GoToLast()

# set active source
SetActiveSource(READER)

# set scalar coloring
ColorBy(READER_DISPLAY, ('CELLS', 'Milieu:Pressure'))

# Hide the scalar bar for this color map if no visible data is colored by it.
HideScalarBarIfNotNeeded(VTK_BLOCK_COLORS_LUT, RENDER_VIEW)

# rescale color and/or opacity maps used to include current data range
READER_DISPLAY.RescaleTransferFunctionToDataRange(True, False)

# show color bar/color legend
READER_DISPLAY.SetScalarBarVisibility(RENDER_VIEW, True)

#================================================================
# addendum: following script captures some of the application
# state to faithfully reproduce the visualization during playback
#================================================================

# get layout
LAYOUT = GetLayout()

#--------------------------------
# saving layout sizes for layouts

# layout/tab size in pixels
LAYOUT.SetSize(1200, 900)

#-----------------------------------
# saving camera placements for views

# current camera placement for renderView1
RENDER_VIEW.InteractionMode = '2D'
RENDER_VIEW.CameraPosition = [0.5, 0.5, 2.7320508075688776]
RENDER_VIEW.CameraFocalPoint = [0.5, 0.5, 0.0]
RENDER_VIEW.CameraParallelScale = 0.7071067811865476

BASELINE_FILE = Path(BASELINE_DIR) / "check_integration_with_cea_annotate_time_filter.png"
Testing.compareImage(RENDER_VIEW.GetRenderWindow(), BASELINE_FILE.as_posix(), threshold=0.0)
Testing.interact()
